import { lazy } from 'react';

// PAGE
const DashboardMain = lazy(() => import('./pages/landing_page/Dashboard'))
const Menu2 = lazy(() => import('./pages/landing_page/Menu2'))
const Menu3 = lazy(() => import('./pages/landing_page/Menu3'))

const routes = [
  { path: '/', key: 'dashboard_main', exact: false, name: 'Dashboard', element: <DashboardMain /> },
  { path: '/menu2', key: 'menu2', exact: true, name: 'Menu 2', element: <Menu2 /> },
  { path: '/menu3', key: 'menu3', exact: true, name: 'Menu 3', element: <Menu3 /> },
];

export default routes;
