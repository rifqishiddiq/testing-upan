import { Header } from "../../components/Header";

const Menu3 = () => {
  return (
    <div>
      <div className="m-2 md:m-8 mt-20 p-2 md:p-8 bg-white rounded-3xl">
        <Header category="Menu 3" title="Menu 3" />
      </div>
    </div>
  );
};

export default Menu3;